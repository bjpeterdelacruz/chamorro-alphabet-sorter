package dev.bjdelacruz.chamorrolanguage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * This class contains a {@link #main(String[])} method that will sort a list of Chamorro words that are stored in a
 * CSV file.
 *
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class ChamorroAlphabetSorter {

    private ChamorroAlphabetSorter() {
    }

    private static final String CHAMORRO_ALPHABET = "A Å B CH D E F G H I K L M N Ñ NG O P R S T U Y";

    private static final String CHAMORRO_RULES = ("< ' < a,A < å,Å < b,B < ch,Ch < d,D < e,E < f,F < g,G < h,H < i,I "
            + "< k,K < l,L < m,M < n,N < ñ,Ñ < ng,Ng < o,O < p,P < r,R < s,S < t,T < u,U < y,Y < -");
    private static final RuleBasedCollator COLLATOR;
    static {
        try {
            COLLATOR = new RuleBasedCollator(CHAMORRO_RULES);
        }
        catch (ParseException pe) {
            throw new RuntimeException(pe);
        }
    }

    /**
     * Sorts a list of Chamorro words that are stored in a CSV file and writes the results to a new CSV file.
     *
     * @param args None
     * @throws IOException If there are problems reading from or writing to files.
     */
    public static void main(String[] args) throws IOException {
        var lines = new ArrayList<String>();
        try (var br = new BufferedReader(new FileReader("src/main/resources/dictionary-corrected.csv",
                StandardCharsets.UTF_8))) {
            var line = br.readLine();
            while (line != null) {
                if (line.equals(",,,,,")) {
                    continue;
                }
                lines.add(line);
                line = br.readLine();
            }
        }

        var treeMap =
                Arrays.stream(CHAMORRO_ALPHABET.split(" ")).collect(
                        Collectors.toMap(String::toString,
                                _ -> new ArrayList<String>(), (string, _) -> string, LinkedHashMap::new));

        lines.forEach(line -> {
            var character = (line.charAt(0) + "").toUpperCase(Locale.US);
            if (line.toUpperCase(Locale.US).startsWith("CH")) {
                treeMap.get("CH").add(line);
            }
            else if (line.toUpperCase(Locale.US).startsWith("NG")) {
                treeMap.get("NG").add(line);
            }
            else if (line.startsWith("-")) {
                character = (line.charAt(1) + "").toUpperCase(Locale.US);
                treeMap.get(character).add(line);
            }
            else {
                treeMap.get(character).add(line);
            }
        });

        treeMap.forEach((_, v) -> v.sort((lhs, rhs) -> COLLATOR.compare(lhs.toUpperCase(Locale.US),
                rhs.toUpperCase(Locale.US))));

        var sortedLines = new ArrayList<String>();
        treeMap.forEach((_, v) -> sortedLines.addAll(v));

        var outputFile = new File("dictionary-sorted.csv");
        var path = Paths.get(outputFile.toURI());
        Files.write(path, sortedLines, StandardCharsets.UTF_8);
    }
}
