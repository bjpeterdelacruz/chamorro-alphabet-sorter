/**
 * Contains the {@link dev.bjdelacruz.chamorrolanguage.ChamorroAlphabetSorter} class for sorting a list of Chamorro
 * words in a file.
 *
 * @author BJ Peter Dela Cruz
 * @since 1.0
 */
package dev.bjdelacruz.chamorrolanguage;